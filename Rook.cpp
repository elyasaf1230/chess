#include "Rook.h"


/*
	Constructor.

	INPUT:
		point place - the start place of the piece.
		string name - the name of the piece.
		group - 1 - group 1.
				2 - group 2.

	OUTPUT:
		none.
*/
Rook::Rook(Point place, string name, int group)
{
	this->_group = group;
	this->_name = name;
	this->_place = place;
}


/*
	Distructor.

	INPUT:
		none.

	OUTPUT:
		none.
*/
Rook::~Rook()
{

}


/*
	This function checking is the request movin is valid.

	INPUT:
		Point dest - the destination.

	OUTPUT:
		true - if movment is valid.
		false - if the movment is not valid.
*/
bool Rook::Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest)
{
	bool ret = true;
	int distanceX, distanceY;

	if (this->_place.getY() == dest.getY())//Sideways
	{
		distanceX = this->_place.getX() - dest.getX();

		for (distanceX -= int(copysign(1, distanceX)); distanceX != 0; distanceX -= int(copysign(1, distanceX)))
		{
			if (table[this->_place.getY()][this->_place.getX() - distanceX])
			{
				ret = false;
			}
		}
	}
	
	else if (this->_place.getX() == dest.getX())//Straight
	{
		distanceY = this->_place.getY() - dest.getY();

		for (distanceY -= int(copysign(1, distanceY)); distanceY != 0; distanceY -= int(copysign(1, distanceY)))
		{
			if (table[this->_place.getY() - distanceY][this->_place.getX()])
			{
				ret = false;
			}
		}
	}
	else
	{
		ret = false;
	}

	return ret;
}