#ifndef _PIECE_H
#define _PIECE_H

#include "Point.h"
#include <string>
#include <math.h>

#define TABLE_LENGHT 8

using namespace std;

class Piece
{
protected:
	
	string _name;
	

public:
	int _group;
	Point _place;
	virtual bool Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest) = 0;

	string GetName() { return _name; };
};

#endif