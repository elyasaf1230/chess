#ifndef _KING_H
#define _KING_H

#include "Piece.h"

class King : public Piece
{
public:
	King(Point place, string name, int group);
	~King();
	virtual bool Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest);
};

#endif