#include "Knight.h"


/*
	Constructor.

	INPUT:
		point place - the start place of the piece.
		string name - the name of the piece.
		group - 1 - group 1.
				2 - group 2.

	OUTPUT:
		none.
*/
Knight::Knight(Point place, string name, int group)
{
	this->_group = group;
	this->_name = name;
	this->_place = place;
}


/*
	Distructor.

	INPUT:
		none.

	OUTPUT:
		none.
*/
Knight::~Knight()
{

}


/*
	This function checking is the request movin is valid.

	INPUT:
		Point dest - the destination.

	OUTPUT:
		true - if movment is valid.
		false - if the movment is not valid.
*/
bool Knight::Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest)
{
	bool ret;

	int distanceX = abs(this->_place.getX() - dest.getX());
	int distanceY = abs(this->_place.getY() - dest.getY());

	if ((distanceX == 2 && distanceY == 1) || (distanceX == 1 && distanceY == 2))
	{
		ret = true;
	}
	else
	{
		ret = false;
	}

	return ret;
}