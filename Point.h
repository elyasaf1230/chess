#ifndef _POINT_H
#define _POINT_H

class Point
{
private:
	int _x;
	int _y;

public:
	//Constructor
	Point();
	Point(int x, int y);
	Point(const Point& other);

	//Distructor
	~Point();

	//getters
	int getX();
	int getY();

	void move(int x, int y);
	void move(const Point& other);

	bool operator==(Point& other);

//	int distance(const Point& other);
};

#endif