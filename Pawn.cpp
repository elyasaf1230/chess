#include "Pawn.h"


/*
	Constructor.

	INPUT:
		point place - the start place of the piece.
		string name - the name of the piece.
		group - 1 - group 1.
				2 - group 2.

	OUTPUT:
		none.
*/
Pawn::Pawn(Point place, string name, int group)
{
	this->_group = group;
	this->_name = name;
	this->_place = place;
}


/*
	Distructor.

	INPUT:
		none.

	OUTPUT:
		none.
*/
Pawn::~Pawn()
{
}


/*
	This function checking if the request movin is valid.

	INPUT:
		Point dest - the destination.

	OUTPUT:
		true - if movment is valid.
		false - if the movment is not valid.
*/
bool Pawn::Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest)
{ 
	bool ret = false;
	//we will separate all the cases to two cases:
	//1. move (can only go 1 step on axis Y).
	if (this->_place.getX() == dest.getX() && !table[dest.getY()][dest.getX()])
	{
		if (this->_place.getY() + (1 * this->_group) == dest.getY())
		{
			ret = true;
		}
		else if (this->_place.getY() + (2 * this->_group) == dest.getY())
		{
			if ((this->_place.getY() == 1 && this->_group == 1) || (this->_place.getY() == 6 && this->_group == -1))
			{
				ret = true;
			}
		}
	}
	//2. eat (go 1 one axis Y and 1 or -1 on axis x - only if there is a piece of the other group there).
	else if (table[dest.getY()][dest.getX()] != NULL &&
		(this->_place.getX() + 1 == dest.getX() || this->_place.getX() - 1 == dest.getX()) &&
		(this->_place.getY() + (1 * this->_group) == dest.getY()))
	{
		ret = true;
	}
	return ret;
}