#ifndef _PAWN_H
#define _PAWN_H

#include "Piece.h"

class Pawn : public Piece
{
public:
	Pawn(Point place, string name, int group);
	~Pawn();
	virtual bool Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest);
};

#endif