#ifndef _QUEEN_H
#define _QUEEN_H

#include "Piece.h"
#include "Rook.h"
#include "Bishop.h"

class Queen : public Piece
{
public:
	Queen(Point place, string name, int group);
	~Queen();
	virtual bool Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest);
};

#endif