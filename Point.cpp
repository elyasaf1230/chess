#include "Point.h"
#include <math.h>


/*
	Constructor.

	INPUT:
		none.

	OUTPUT:
		none.
*/
Point::Point()
{
}


/*
	Constructor.

	INPUT:
		double x - x value of the point.
		double y - y value of the point.

	OUTPUT:
		none.
*/
Point::Point(int x, int y)
{
	this->_x = x;
	this->_y = y;
}


/*
	Copy Constructor.

	INPUT:
		point - point to copy.

	OUTPUT:
		none.
*/
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}


/*
	Distructor.

	INPUT:
		none.

	OUTPUT:
		none.
*/
Point::~Point()
{
}


/*
	This function returning the x value of the point.

	INPUT:
		none.

	OUTPUT:
		double - the value of x of the point.
*/
int Point::getX()
{
	return this->_x;
}


/*
	This function returning the y value of the point.

	INPUT:
		none.

	OUTPUT:
		double - the value of x of the point.
*/
int Point::getY()
{
	return this->_y;
}


/*
	This function 'moving' the current point accurding to the given x and y values.

	INPUT:
		double x - the new x value of the point.
		double y - the new y value of the point.

	OUTPUT:
		none.
*/
void Point::move(int x, int y)
{
	this->_x = x;
	this->_y = y;
}


/*
	This function 'moving' the current point to the given point.

	INPUT:
		point - the point to move to the current point.

	OUTPUT:
		none.
*/
void Point::move(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}


/*
	This function calculate the distance between the currnet point and the given point.

	INPUT:
		point.

	OUTPUT:
		double - the distance between the points.
*/
//int Point::distance(const Point& other)
//{
//	return sqrt(pow(this->_x - other._x, 2) + pow(this->_y - other._y, 2));
//}

/*
	operator !=.
*/
bool Point::operator==(Point& other)
{
	return (this->_x == other._x && this->_y == other._y);
}