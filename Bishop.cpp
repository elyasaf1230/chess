#include "Bishop.h"


/*
	Constructor.

	INPUT:
		point place - the start place of the piece.
		string name - the name of the piece.
		group - 1 - group 1.
				2 - group 2.

	OUTPUT:
		none.
*/
Bishop::Bishop(Point place, string name, int group)
{
	this->_place = place;
	this->_name = name;
	this->_group = group;
}


/*
	Distructor.

	INPUT:
		none.

	OUTPUT:
		none.
*/
Bishop::~Bishop()
{
}


/*
	This function checking is the request movin is valid.

	INPUT:
		Point dest - the destination.

	OUTPUT:
		true - if movment is valid.
		false - if the movment is not valid.
*/
bool Bishop::Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest)
{
	bool ret = true;
	int distanceX, distanceY;
	//move and eat(can only go x steps on axis X and x steps on axis Y).

	if (abs(this->_place.getX() - dest.getX()) == abs(this->_place.getY() - dest.getY()))
	{
		//for move and eat both, can't be pieces between the current piece and the destination place.

		distanceX = this->_place.getX() - dest.getX();
		distanceY = this->_place.getY() - dest.getY();

		for (distanceX -= int(copysign(1, distanceX)), distanceY -= int(copysign(1, distanceY));
			 distanceX != 0 && distanceY != 0;
			 distanceX -= int(copysign(1, distanceX)), distanceY -= int(copysign(1, distanceY)))
		{
			if (table[this->_place.getY() - distanceY][this->_place.getX() - distanceX])
			{
				ret = false;
			}
		}
	}
	else
	{
		ret = false;
	}

	return ret;
}