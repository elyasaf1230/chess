#include "Table.h"

using namespace std;

void main()
{
	string player1, player2;
	Table game_board = Table();
	Point start, dest;
	int x;
	char y;

	cout << "Welcome to Michal and Chilick Chess Game!" << endl;

	cout << "Please enter name of player one here :" << endl;
	cin >> player1;
	cout << "Please enter name of player two here :" << endl;
	cin >> player2;
	
	while (true)
	{
		//print board.
		game_board.PrintTable();

		//queue check.
		if (game_board.GetQueue())
		{
			cout << player1 << ":" << endl;	
		}
		else
		{
			cout << player2 << ":" << endl;
		}

		//taking points.
		cout << "Please insert the coordinates of the piece you want to move : ";
		cin >> x >> y;
		start = Point(x, y - 'A');

		cout << "Please insert the coordinates of the square you want to move to : ";
		cin >> x >> y;
		dest = Point(x, y - 'A');

		//move and check if valid.
		if (!game_board.Move(start, dest))
		{
			cout << "invalid movment, please try again." << endl;
		}
	}

	system("PAUSE");
}