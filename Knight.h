#ifndef _KNIGHT_H
#define _KNIGHT_H

#include "Piece.h"

class Knight : public Piece
{
public:
	Knight(Point place, string name, int group);
	~Knight();
	virtual bool Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest);
};

#endif