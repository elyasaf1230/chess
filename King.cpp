#include "King.h"


/*
	Constructor.

	INPUT:
		point place - the start place of the piece.
		string name - the name of the piece.
		group - 1 - group 1.
				2 - group 2.

	OUTPUT:
		none.
*/
King::King(Point place, string name, int group)
{
	this->_place = place;
	this->_name = name;
	this->_group = group;
}


/*
	Distructor.

	INPUT:
		none.

	OUTPUT:
		none.
*/
King::~King()
{
}


/*
	This function checking is the request movin is valid.

	INPUT:
		Point dest - the destination.

	OUTPUT:
		true - if movment is valid.
		false - if the movment is not valid.
*/
bool King::Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest)
{
	bool ret;
	if (abs(this->_place.getX() - dest.getX()) <= 1 && abs(this->_place.getY() - dest.getY()) <= 1)
	{
		ret = true;
	}
	else
	{
		ret = false;
	}

	return ret;
}