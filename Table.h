#ifndef _TABLE_H
#define _TABLE_H

#include <iostream>
#include <string>
#include "Point.h"
#include "Piece.h"

#include "King.h"
#include "Queen.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Pawn.h"

#define TABLE_LENGHT 8
#define GROUPA 1
#define GROUPB -1

#define KING 4

class Table
{
private:
	Piece* _table[TABLE_LENGHT][TABLE_LENGHT];
	
	Piece* _group1[TABLE_LENGHT * 2];
	Piece* _group2[TABLE_LENGHT * 2];

	bool _chess1 = false;              //chess on group1's king.
	bool _chess2 = false;			  //chess on group2's king.	

	bool _queue = true;               //true - queue of the first group.
								   	 //false - queue of the second group.

	bool chess(Point start, Point dest);
	void Replace(Point start, Point dest);
	bool Threat(Point dest);

public:
	Table();
	~Table();

	bool Move(Point start, Point dest);
	void PrintTable();
	bool GetQueue();
};

#endif