#ifndef _ROOK_H
#define _ROOK_H

#include "Piece.h"

class Rook : public Piece
{
public:
	Rook(Point place, string name, int group);
	~Rook();
	virtual bool Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest);
};

#endif