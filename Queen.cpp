#include "Queen.h"


/*
	Constructor.

	INPUT:
		point place - the start place of the piece.
		string name - the name of the piece.
		group - 1 - group 1.
				2 - group 2.

	OUTPUT:
		none.
*/
Queen::Queen(Point place, string name, int group)
{
	this->_group = group;
	this->_name = name;
	this->_place = place;
}


/*
	Distructor.

	INPUT:	
		none.

	OUTPUT:
		none.
*/
Queen::~Queen()
{
}


/*
	This function checking is the request movin is valid.

	INPUT:
		Point dest - the destination.

	OUTPUT:
		true - if movment is valid.
		false - if the movment is not valid.
*/
bool Queen::Move(Piece* table[TABLE_LENGHT][TABLE_LENGHT], Point dest)
{
	Rook Rcheck = Rook(this->_place, "check", this->_group);
	Bishop Bcheck = Bishop(this->_place, "check", this->_group);

	return(Rcheck.Move(table,dest) || Bcheck.Move(table,dest));
}