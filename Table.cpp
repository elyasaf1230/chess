#include "Table.h"


/*
	Constructor.

	INPUT:
		none.

	OUTPUT:
		none.
*/
Table::Table()
{
	for (int i = 0; i < TABLE_LENGHT; i++)
	{
		for (int j = 0; j < TABLE_LENGHT; j++)
		{
			this->_table[i][j] = NULL;
		}
	}

	//Group A: 
	//table.
	this->_table[0][0] = new Rook(Point(0, 0), "Rook", GROUPA);
	this->_table[0][1] = new Knight(Point(1, 0), "Horse", GROUPA);
	this->_table[0][2] = new Bishop(Point(2, 0), "Bishop", GROUPA);
	this->_table[0][3] = new Queen(Point(3, 0), "Queen", GROUPA);
	this->_table[0][4] = new King(Point(4, 0), "King", GROUPA);
	this->_table[0][5] = new Bishop(Point(5, 0), "Bishop", GROUPA);
	this->_table[0][6] = new Knight(Point(6, 0), "Horse", GROUPA);
	this->_table[0][7] = new Rook(Point(7, 0), "Rook", GROUPA);
	for (int i = 0; i < TABLE_LENGHT; i++)
	{
		this->_table[1][i] = new Pawn(Point(i, 1), "Pawn", GROUPA);
	}

	//array.
	for (int a = 0, i = 0; i <= 1; i++)
	{
		for (int j = 0; j < TABLE_LENGHT; j++, a++)
		{
			this->_group1[a] = this->_table[i][j];
		}
	}


	//Group B:
	//table.
	_table[7][0] = new Rook(Point(0, 7), "Rook", GROUPB);
	_table[7][1] = new Knight(Point(1, 7), "Horse", GROUPB);
	_table[7][2] = new Bishop(Point(2, 7), "Bishop", GROUPB);
	_table[7][4] = new King(Point(4, 7), "King", GROUPB);
	_table[7][3] = new Queen(Point(3, 7), "Queen", GROUPB);
	_table[7][5] = new Bishop(Point(5, 7), "Bishop", GROUPB);
	_table[7][6] = new Knight(Point(6, 7), "Horse", GROUPB);
	_table[7][7] = new Rook(Point(7, 7), "Rook", GROUPB);

	for (int i = 0; i < TABLE_LENGHT; i++)
	{
		_table[6][i] = new Pawn(Point(i, 6), "Pawn", GROUPB);
	}


	//array.
	for (int a = 0, i = 7; i >= 6; i--)
	{
		for (int j = 0; j < TABLE_LENGHT; j++, a++)
		{
			this->_group2[a] = this->_table[i][j];
		}
	}
}


/*
	Distructor.

	INPUT:
		none.

	OUTPUT:
		none.
*/
Table::~Table()
{
	//first, we will put nulls in the table so the pieces will
	//be accessable only from the _group1 and _group2.
	for (int i = 0; i < TABLE_LENGHT; i++)
	{
		for (int j = 0; j < TABLE_LENGHT; j++)
		{
			if (_table[i][j])
			{
				_table[i][j] = NULL;
			}
		}
	}

	//now, we will delete the pieces from _group1 and _group2.
	for (int i = 0; i < TABLE_LENGHT * 2; i++)
	{
		if (_group1[i])
		{
			delete _group1[i];
		}
		
		if (_group2[i])
		{
			delete _group2[i];
		}
	}
}


/*
	This function moving the piece in the first given point 
	to the second given point (if possible).

	INPUT:
		Point start - the place of the piece to move.
		Point dest - the destination.

	OUTPUT:
		true - if succeeded to move the piece to the destination,
			   if not - false.
*/
bool Table::Move(Point start, Point dest)
{
	bool ret;

	if (this->_table[start.getY()][start.getX()] && this->_queue == !!(this->_table[start.getY()][start.getX()]->_group + 1))
	{
		if (start.getX() >= 0 && start.getX() < TABLE_LENGHT && start.getY() >= 0 && start.getY() < TABLE_LENGHT &&
			dest.getX() >= 0 && dest.getX() < TABLE_LENGHT && dest.getY() >= 0 && dest.getY() < TABLE_LENGHT &&
			(!this->_table[dest.getY()][dest.getX()] ||
			this->_table[dest.getY()][dest.getX()]->_group != this->_table[start.getY()][start.getX()]->_group))
		{
			if (this->_table[start.getY()][start.getX()]->Move(this->_table, dest) && !(this->chess(start, dest)))
			{
				//move succeeded.
				ret = true;

				//move from start to dest.
				this->Replace(start, dest);

				//changing queue.
				this->_queue = !this->_queue;

				//updating chess.
				if (this->_table[dest.getY()][dest.getX()]->_group == GROUPA)
				{
					this->_chess1 = false;
					this->_chess2 = this->Threat(this->_group2[KING]->_place);
				}
				else if (this->_table[dest.getY()][dest.getX()]->_group == GROUPB)
				{
					this->_chess1 = this->Threat(this->_group1[KING]->_place);
					this->_chess2 = false;
				}
			}
			else
			{
				ret = false;
			}
		}
		else
		{
			ret = false;
		}
	}
	else
	{
		ret = false;
	}

	if (_chess1)
	{
		std::cout << "chess on group 1!" << std::endl;
	}
	else if (_chess2)
	{
		std::cout << "chess on group 2!" << std::endl;
	}

	return ret;
}


/*
	This function printing the table.

	INPUT:
		none.

	OUTPUT:
		none.
*/
void Table::PrintTable()
{
	cout << "  ";
	for (int k = 0; k < TABLE_LENGHT; k++)
	{
		cout << "   " << k;
	}
	cout << endl << endl;

	cout << "    --- --- --- --- --- --- --- ---" << endl;

	for (int i = 0; i < TABLE_LENGHT; i++)
	{
		cout << char(i + 'A') << "  | ";
		for (int j = 0; j < TABLE_LENGHT; j++)
		{
			if (this->_table[i][j])
			{
				if (this->_table[i][j]->_group == 1)
				{
					cout << _table[i][j]->GetName()[0] << " | ";
				}
				else
				{
					cout << char(_table[i][j]->GetName()[0] + 32) << " | ";
				}
				
			}
			else
			{
				std::cout << "  | ";
			}
		}
		std::cout << std::endl << "    --- --- --- --- --- --- --- ---" << endl;
	}
}

/*
	This function checks if the new move will commit a chess on the piece's group 
	or if there is a chess and the new move won't handle it.

	Input:
		point - the start place
		point - the destination

	Output:
		true - if chess
		false - if not chess
*/

bool Table::chess(Point start , Point dest)
{
	//Need to do 2 checks:
	//1.If the new move would commit a chess on the current group.
	//2.If there is a chess on the current group and the new move don't handle it.

	// we are assuming that after the current piece move to dest, there won't be chess on it's group. (so check1 and check2 are the same check...) 
	bool ret = false;

	Piece * temp = NULL;
	int pInG = 0;

	//making the move.
	if (!(start == dest))
	{
		this->_table[start.getY()][start.getX()]->_place = dest;							//update point.

		temp = this->_table[dest.getY()][dest.getX()];										//save the data in dest.
		this->_table[dest.getY()][dest.getX()] = this->_table[start.getY()][start.getX()];	//moving start to dest.
		this->_table[start.getY()][start.getX()] = NULL;									//clear dest.

		if (temp && temp->_group == GROUPA)						//
		{																					//
			for (int i = 0; i < TABLE_LENGHT * 2; i++)										//
			{																				//
				if (this->_group1[i] == temp)												//
				{																			// temporary
					pInG = i;																//			clean
					this->_group1[i] = NULL;												//				the 
				}																			//					data
			}																				//				from
		}																					//			array
		else if (temp) //GROUPB																//		group
		{																					//
			for (int i = 0; i < TABLE_LENGHT * 2; i++)										//
			{																				//
				if (this->_group2[i] == temp)												//
				{																			//
					pInG = i;																//
					this->_group2[i] = NULL;												//
				}																			//
			}																				//
		}																					//
	}

	//check 1.
	ret = Threat(dest);

	/*
	//check 2.
	//check if there is a threat on group1 and the new move cancel it.
	if (this->_chess1 && this->_table[dest.getY()][dest.getX()]->_group == GROUPA) //if chess1 - chess on group1.  
	{
		ret = !Threat(this->_group2[KING]->_place);
	}
	//check if there is a threat on group2 and the new move cancel it.
	else if (this->_chess2 && this->_table[dest.getY()][dest.getX()]->_group == GROUPB)
	{
		ret = !Threat(this->_group1[KING]->_place);
	}
	*/

	if (!(start == dest))
	{
		this->_table[dest.getY()][dest.getX()]->_place = start;								//recover data of point.

		this->_table[start.getY()][start.getX()] = this->_table[dest.getY()][dest.getX()];	//return piece from dest to start
		
		this->_table[dest.getY()][dest.getX()] = temp;										//recover data of dest.

		if (temp && temp->_group == GROUPA)													//
		{																					//return 
			this->_group1[pInG] = temp;														//		data
		}																					//			to 
		else //GROUPB																		//		it's 
		{																					//group
			this->_group2[pInG] = temp;														//
		}																					//
	}

	return ret;
}


/*
	This function checking if there is a chess on the dest's group.

	INPUT:
		point - the point of the piece.

	OUTPUT:
		true - if there is a threat, else - false.
*/
bool Table::Threat(Point dest)
{
	bool ret = false;
	if (this->_table[dest.getY()][dest.getX()]->_group == GROUPA)
	{
		for (int i = 0; i < TABLE_LENGHT * 2; i++)
		{
			if (this->_group2[i] && this->_group2[i]->Move(this->_table, this->_group1[KING]->_place))
			{
				ret = true;
			}
		}
	}
	else if (this->_table[dest.getY()][dest.getX()]->_group == GROUPB)
	{
		for (int i = 0; i < TABLE_LENGHT * 2; i++)
		{
			if (this->_group1[i] && this->_group1[i]->Move(this->_table, this->_group2[KING]->_place))
			{
				ret = true;
			}
		}
	}

	return ret;
}

/*
	This function moves the piece in start to dest
	and deletes the piece in dest : P

	Input:
		point - start
		point - dest

	Output:
		none

*/
void Table :: Replace(Point start, Point dest)
{
	this->_table[start.getY()][start.getX()]->_place = Point(dest.getX(), dest.getY());//new place
	
	if (this->_table[dest.getY()][dest.getX()])
	{

		if (this->_table[dest.getY()][dest.getX()]->_group == GROUPA)
		{
			for (int i = 0; i < TABLE_LENGHT * 2; i++)
			{
				if (this->_table[dest.getY()][dest.getX()] == this->_group1[i])
				{
					this->_table[dest.getY()][dest.getX()] = NULL;
					delete this->_group1[i];
					this->_group1[i] = NULL;
				}
			}
		}
		else if (this->_table[dest.getY()][dest.getX()]->_group == GROUPB)
		{
			for (int i = 0; i < TABLE_LENGHT * 2; i++)
			{
				if (this->_table[dest.getY()][dest.getX()] == this->_group2[i])
				{
					this->_table[dest.getY()][dest.getX()] = NULL;
					delete this->_group2[i];
					this->_group2[i] = NULL;
				}
			}
		}

	}

	this->_table[dest.getY()][dest.getX()] = this->_table[start.getY()][start.getX()];//move piece
	this->_table[start.getY()][start.getX()] = NULL;

}

//return queue
bool Table::GetQueue()
{
	return this->_queue;
}